var element = document.querySelector('header._21UZG');

window.onscroll = function () {
    if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50 || window.pageYOffset >50) {
        element.style.display = 'none';
    } else {
        element.style.display = 'block';
    }
}