Chrome addon for removing the top element from the Sydney Morning Herald Website

To finalise addon, turn the "SMH Says What" folder into a zip and upload to Chrome Extension

To run as developer mode:
1. Type in: chrome://extensions/ in the Chrome browser
2. Swith on developer mode on the top right of the page
3. Click on the "Load Unpacked" tab on the top middle of page
4. Load the "SMH Says What" folder into chrome.
5. Test


I've left jquery in, incase anyone needs to use it.

Have Fun!